import React from 'react';
import {Panel} from "react-bootstrap";

import './Message.css';

const Message = props => (
  <div className='Message'>
    <Panel bsStyle="primary">
      <Panel.Heading>
        <Panel.Title componentClass="h3">
          Author: {props.author}
        </Panel.Title>
      </Panel.Heading>
      <Panel.Body>
        <span className='Message-Date'>Date: {props.date}</span>
        <p className='Message-Text'>{props.message}</p>
      </Panel.Body>
    </Panel>
  </div>
);

export default Message;