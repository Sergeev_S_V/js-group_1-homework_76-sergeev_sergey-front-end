import React, {Component} from 'react';
import {Button, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {connect} from "react-redux";
import {sendMessage} from "../../store/actions";

class AddForm extends Component {

  state = {
    author: '',
    message: '',
  };

  changeInputHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  sendMessageHandler = event => {
    event.preventDefault();

    let message = {
      author: this.state.author,
      message: this.state.message,
    };

    this.props.onSendMessage(message);

  };

  render() {
    return (
      <div className='AddForm'>
        <Form inline onSubmit={(event) => this.sendMessageHandler(event)}>
          <FormGroup controlId="formInlineMessage">
            <ControlLabel>Message</ControlLabel>{' '}
            <FormControl required
                         onChange={this.changeInputHandler}
                         value={this.state.message}
                         type="text"
                         name='message'
                         placeholder="Message"/>
          </FormGroup>{' '}
          <FormGroup controlId="formInlineAuthor">
            <ControlLabel>Author</ControlLabel>{' '}
            <FormControl required
                         onChange={this.changeInputHandler}
                         value={this.state.author}
                         type="text"
                         name='author'
                         placeholder="Author"/>
          </FormGroup>{' '}
          <Button type="submit">
            Send message
          </Button>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSendMessage: message => dispatch(sendMessage(message))
  }
};



export default connect(null, mapDispatchToProps)(AddForm);