import React, { Component } from 'react';
import './App.css';
import AddForm from "./Components/AddForm/AddForm";
import Messages from "./Containers/Messages/Messages";

class App extends Component {
  render() {
    return (
      <div className="App">
        <AddForm/>
        <Messages/>
      </div>
    );
  }
}

export default App;
