import axios from '../axios-api';

import {SUCCESS_FETCH_MESSAGES} from "./actionTypes";

export const successFetchMessages = (messages, lastDate) => {
  return {type: SUCCESS_FETCH_MESSAGES, messages, lastDate};
};

export const fetchMessages = (lastDate) => dispatch => {
  if (lastDate) {
    axios.get(`/messages?datetime=${lastDate}`)
      .then(resp => {
        if (resp.data.length > 0) {
          lastDate = resp.data[resp.data.length - 1].datetime;
          dispatch(successFetchMessages(resp.data, lastDate));
        }
      })
  } else {
    axios.get('/messages')
      .then(resp => {
        if (resp) {
          let lastDate = resp.data[resp.data.length - 1].datetime;
          dispatch(successFetchMessages(resp.data, lastDate));
        }
      });
  }



};

export const sendMessage = message => async dispatch => {
  return await axios.post('/messages', message)
    .then((resp) => console.log(resp));
};
