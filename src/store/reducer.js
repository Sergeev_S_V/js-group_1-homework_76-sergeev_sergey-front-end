import {SUCCESS_FETCH_MESSAGES} from "./actionTypes";

const initialState = {
  messages: [],
  lastDate: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_FETCH_MESSAGES:
      return {...state, messages: state.messages.concat(action.messages), lastDate: action.lastDate};
    default:
      return state;
  }
};

export default reducer;