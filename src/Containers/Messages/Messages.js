import React, {Component} from 'react';
import {Col, Grid, Row} from "react-bootstrap";
import {connect} from "react-redux";

import {fetchMessages} from "../../store/actions";
import Message from "../../Components/Message/Message";
import './Messages.css';

class Messages extends Component {

  interval = null;

  componentDidMount() {
    this.props.onFetchMessages();
    this.interval = setInterval(() => {
      console.log('2 sec have gone');
      this.props.onFetchMessages(this.props.lastDate);
    }, 5000);
  };

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    let messagesList;

    if (this.props.messages.length === 0) {
      messagesList = <div>Messages list is empty</div>
    } else {
      messagesList = (
        <Col lg={12}>
          {this.props.messages.map(message => (
            <Message key={message.id}
                     author={message.author}
                     message={message.message}
                     date={message.datetime}
            />
          ))}
        </Col>
      );
    }

    return (
      <Grid>
        <Row className="show-grid">
          {messagesList}
        </Row>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messages,
    lastDate: state.lastDate,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchMessages: (lastDate) => dispatch(fetchMessages(lastDate)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);